defmodule Sassaby.MixProject do
  use Mix.Project

  def project do
    [
      app: :sassaby,
      version: "0.1.0",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def application do
    [
      extra_applications: [:logger, :ranch],
      mod: {Sassaby.Application, []}
    ]
  end

  defp deps do
    [
      {:ed25519, "~> 1.3"},
      {:kcl, "~> 1.3"},
      {:jason, "~> 1.2"},
      {:ranch, "~> 2.0"},
      {:earmark, "~> 1.0", only: :dev},
      {:ex_doc, "~> 0.15", only: :dev}
    ]
  end
end
