defmodule SassabyTest do
  use ExUnit.Case
  doctest Sassaby

  test "secret_identity" do
    assert String.at(Sassaby.secret_identity(:base64), -1) == "=", "Looks kinda base64 encoded"
  end

  test "public_identity" do
    assert String.at(Sassaby.public_identity(:base64), -1) == "=", "Looks kinda base64 encoded"
  end
end
