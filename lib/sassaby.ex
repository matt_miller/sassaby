defmodule Sassaby do
  @moduledoc """
  Documentation for Sassaby.
  """
  @settings_dir Path.expand(Application.compile_env(:sassaby, :settings_dir))
  @network_dir Path.join(@settings_dir, "network")
  @default_network Application.compile_env(:sassaby, :network)
  @identity_dir Path.join(@settings_dir, "identity")
  @default_ident Application.compile_env(:sassaby, :identity)

  def network_id(which \\ @default_network), do: File.read!(Path.join(@network_dir, which))

  def create_network_file(<<id::binary-size(32)>>, name),
    do: File.write!(Path.join(@network_dir, name), id)

  def secret_identity(format, ident \\ @default_ident),
    do: read_identity(ident, :private, format)

  def public_identity(format, ident \\ @default_ident),
    do: read_identity(ident, :public, format)

  defp per_ident_path(ident), do: Path.join(@identity_dir, ident)

  defp path_to_ident_file(ident, which),
    do: ident |> per_ident_path |> Path.join(Atom.to_string(which))

  defp read_identity(ident, which, format) do
    val =
      case File.read(path_to_ident_file(ident, which)) do
        {:ok, key} ->
          key

        {:error, :enoent} ->
          {secret, public} =
            case File.read(Path.join(per_ident_path(ident), "secret")) do
              {:ok, contents} ->
                import_identity(ident, contents)

              {:error, :enoent} ->
                make_identity(ident)
            end

          case which do
            :private -> secret
            :public -> public
          end
      end

    case format do
      :base64 -> val
      :raw -> val |> Base.decode64!()
      :hex -> val |> Base.decode64!() |> Base.encode16(case: :lower)
    end
  end

  defp import_identity(ident, json_file) do
    val_map =
      json_file
      |> String.split("\n")
      |> Enum.reject(fn s -> s =~ ~r/^#/ end)
      |> Enum.join("")
      |> Jason.decode!()

    # If key types switch, we'll need to know
    make_id_file(ident, :private, without_curve(val_map, "private"))
    make_id_file(ident, :public, without_curve(val_map, "public"))

    {val_map["private"], val_map["public"]}
  end

  defp without_curve(map, key), do: String.replace_trailing(map[key], "." <> map["curve"], "")

  defp make_id_file(ident, which, contents) do
    file = path_to_ident_file(ident, which)
    :ok = File.write(file, contents)

    mode =
      case which do
        :private -> 0o600
        :public -> 0o644
      end

    :ok = File.chmod(file, mode)
  end

  defp make_identity(ident) do
    :ok = File.mkdir_p(per_ident_path(ident))
    {raw_secret, raw_public} = Ed25519.generate_key_pair()
    secret = Base.encode64(raw_secret)
    public = Base.encode64(raw_public)
    make_id_file(ident, :private, secret)
    make_id_file(ident, :public, public)
    {secret, public}
  end

  def peer_status(id) do
    case :ets.lookup(:peers, id) do
      [{_, "bad"}] ->
        :ignore

      [{_, then}] ->
        # This peer timeout should be configurable
        case Time.compare(Time.utc_now(), Time.add(then, 311, :second)) do
          :gt -> :ok
          _ -> :ignore
        end

      [] ->
        :ok
    end
  end
end
