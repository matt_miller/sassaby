defmodule Sassaby.Application do
  @moduledoc false

  use Application
  import Supervisor.Spec, warn: false

  def start(_app, _args) do
    :ets.new(:peers, [:named_table, :public])
    # Treat ourselves as a bad peer.
    :ets.insert(:peers, {Sassaby.public_identity(:base64), "bad"})

    :ranch.start_listener(
      :gossip,
      :ranch_tcp,
      [port: Application.get_env(:sassaby, :port)],
      Sassaby.Connection.Incoming,
      [%{stream: :recv_hello}]
    )

    children = [
      {Registry, keys: :unique, name: Registry.RPCStreams},
      {Sassaby.Discovery, []}
    ]

    opts = [strategy: :one_for_one, name: Sassaby.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
