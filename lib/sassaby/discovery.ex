defmodule Sassaby.Discovery do
  use GenServer
  require Logger

  @moduledoc """
  Local network pub announcements
  """

  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts, name: __MODULE__)
  end

  @impl true
  def init(_arg) do
    port = Application.get_env(:sassaby, :port)
    {tip, sip} = find_addr()

    case :gen_udp.open(8008, [:binary, broadcast: true, reuseaddr: true]) do
      {:ok, _} -> :ok
      {:error, code} -> Logger.warn(["No LAN discovery listener: ", Atom.to_string(code)])
    end

    case :gen_udp.open(port, [:binary, broadcast: true, active: false, ip: tip, reuseaddr: true]) do
      {:ok, socket} ->
        state = [socket, build_packet(sip, port)]
        announce(state)
        {:ok, state}

      {:error, code} ->
        Logger.warn(["LAN discovery off: ", Atom.to_string(code)])
        :ignore
    end
  end

  @impl true
  def handle_info({:udp, _socket, _ip, _port, data}, state) do
    case Regex.named_captures(~r/net:(?<ip>[^:]*):(?<port>[^~]*)~shs:(?<id>[^;]*)/, data) do
      %{"id" => id, "ip" => _, "port" => _} = peer_info ->
        case Sassaby.peer_status(id) do
          :ok ->
            :ets.insert(:peers, {id, Time.utc_now()})
            # Otherside will figure out if the peer is bad or us or whatever
            Logger.debug(["handling announced peer: ", Jason.encode!(peer_info)])
            Sassaby.Connection.Outgoing.start_link(peer_info)

          :ignore ->
            :ok
        end

      # We can't decode the packet, screw it
      _ ->
        :ok
    end

    {:noreply, state}
  end

  @impl true
  def handle_info(:announce, state) do
    announce(state)
    {:noreply, state}
  end

  defp announce([socket, packet]) do
    :ok = :gen_udp.send(socket, {255, 255, 255, 255}, 8008, packet)
    Process.send_after(self(), :announce, 11113)
  end

  defp find_addr do
    case addr_config([:addr, :interface]) do
      {:addr, sip} ->
        {:ok, ip} = :inet.parse_address(sip |> String.to_charlist())
        {ip, sip}

      {:interface, face} ->
        {:ok, addrs} = :net.getifaddrs(:inet)
        face_info(addrs, face)

      {:none, _} ->
        default_addr()
    end
  end

  defp default_addr do
    {:ok, [{ip, _bcast, _nmask} | _]} = :inet.getif()
    {ip, ip |> :inet.ntoa() |> to_string}
  end

  defp addr_config([]), do: {:none, nil}

  defp addr_config([key | rest]) do
    case Application.get_env(:sassaby, key) do
      nil -> addr_config(rest)
      val -> {key, val}
    end
  end

  defp face_info(addrs, face) when is_binary(face), do: face_info(addrs, to_charlist(face))
  # Figure localhost is safest fallback
  defp face_info([], _), do: default_addr()

  defp face_info([addr | rest], face) do
    case Map.get(addr, :name) do
      ^face ->
        ip = addr |> Map.get(:addr) |> Map.get(:addr)
        {ip, ip |> :inet.ntoa() |> to_string}

      _ ->
        face_info(rest, face)
    end
  end

  defp build_packet(ip, port) do
    pubkey = Sassaby.public_identity(:base64)
    "net:" <> ip <> ":" <> Integer.to_string(port) <> "~shs:" <> pubkey
  end
end
