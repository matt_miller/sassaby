defmodule Sassaby.Boxstream do
  require Logger
  alias Sassaby.Util
  @goodbye_packet Sassaby.Util.zeroes(18)
  def process(state) do
    case Map.get(state, :boxstream) do
      :goodbye ->
        Logger.debug([Util.visual_ident(state.their_pk), " boxstream goodbye"])
        Process.send(self(), :close, [])
        state

      :rpc ->
        {:ok, excess} = Sassaby.RPC.process(state)
        process(Map.merge(state, excess))

      :pack ->
        case state.box_data do
          <<header::binary-size(34), rest::binary>> ->
            {cnonce, next_state} = Sassaby.Util.get_nonce(state, :recv_nonce)

            case Kcl.secretunbox(header, cnonce, state.recv_key) do
              :error ->
                Logger.error([Util.visual_ident(state.their_pk), " OUTER UNBOX ERROR"])
                %{state | :boxstream => :goodbye}

              @goodbye_packet ->
                Logger.debug([Util.visual_ident(state.their_pk), " proper goodbye header"])
                %{state | :boxstream => :goodbye}

              <<blen::big-unsigned-size(16), bat::binary-size(16)>> ->
                handle_body(rest, blen, bat, next_state, cnonce)
            end

          data ->
            %{state | :box_data => state.box_data <> data}
        end
    end
  end

  defp handle_body(data, dlen, _bat, state, prev_nonce) when byte_size(data) < dlen do
    %{state | :box_data => state.box_data <> data, :recv_nonce => prev_nonce}
  end

  defp handle_body(data, dlen, bat, %{:rpc_flags => ""} = state, _prev_nonce) do
    unbox(data, dlen, bat, state)
  end

  defp handle_body(data, dlen, bat, %{:rpc_flags => rpc_flags} = state, _prev_nonce) do
    # Now we need to figure out if we have a complete box
    whole = rpc_flags.len

    case state.rpc_data <> data do
      <<_::binary-size(whole), _::binary>> = complete ->
        unbox(complete, whole, bat, state)

      <<body::binary-size(dlen), nextbox::binary>> ->
        %{state | :rpc_data => state.rpc_data <> bat <> body, :box_data => nextbox}

      incomplete ->
        %{state | :rpc_data => incomplete, :box_data => <<>>}
    end
  end

  defp unbox(data, dlen, bat, state) do
    <<body::binary-size(dlen), r::binary>> = data
    {nonce, further_state} = Sassaby.Util.get_nonce(state, :recv_nonce)

    changes =
      case Kcl.secretunbox(bat <> body, nonce, state.recv_key) do
        :error ->
          Logger.error([Util.visual_ident(state.their_pk), " INNER UNBOX ERROR"])
          %{:rpc_data => <<>>}

        fresh_rpc ->
          %{:rpc_data => state.rpc_data <> fresh_rpc, :boxstream => :rpc}
      end

    further_state
    |> Map.merge(changes)
    |> Map.replace(:box_data, r)
    |> process
  end
end
