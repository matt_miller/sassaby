defmodule Sassaby.RPC.Flags do
  defstruct stream: 0,
            stop: 0,
            type: 2,
            len: 0,
            reqno: 0

  @doc """
  More than it claims, this returns a tuple of

  {body length, flags tuple, rest of the binary}
  """
  def from_header_bytes(
        <<_::size(4), stream::size(1), stop::size(1), type::big-size(2),
          len::big-unsigned-size(32), reqno::big-signed-size(32), rest::binary>>
      ) do
    {%__MODULE__{stream: stream, stop: stop, type: type, len: len, reqno: reqno}, rest}
  end

  def from_header_bytes(_), do: :nomatch

  def to_header_bytes(%__MODULE{stream: stream, stop: stop, type: type, len: len, reqno: reqno}) do
    <<0::1, 0::1, 0::1, 0::1, stream::size(1), stop::size(1), type::big-size(2),
      len::big-unsigned-size(32), reqno::big-signed-size(32)>>
  end

  def extract(bytes, key) do
    {flags, _} = from_header_bytes(bytes)
    Map.fetch!(flags, key)
  end
end
