defmodule Sassaby.RPC.Dispatch do
  require Logger
  alias Sassaby.Util
  alias Sassaby.RPC.StreamResponse
  # Streaming requests -- JSON
  def request(
        %{flags: %{stream: 1, stop: stop, reqno: reqno, type: 2} = flags, request: body},
        state,
        {initiator, _source} = direction
      ) do
    procreq =
      case direction do
        {:us, :local} -> reqno * -1
        {:them, :network} -> reqno * -1
        _ -> reqno
      end

    process_name =
      Enum.join(
        [pid_string(self()), Atom.to_string(initiator), Integer.to_string(abs(reqno))],
        "/"
      )

    process =
      case Registry.lookup(Registry.RPCStreams, process_name) do
        [] -> nil
        [{pid, _}] -> pid
      end

    case {stop, process} do
      {1, nil} ->
        debug_log(state, ["stop request on non-runner", reqno])

      {1, pid} ->
        debug_log(state, ["requested stop", reqno, body])
        Process.send(pid, :shutdown, [])

      {0, nil} ->
        debug_log(state, [
          process_name,
          "for",
          body
        ])

        StreamResponse.start_link(
          {flags, body, state |> Map.put(:thisreq, procreq) |> Map.put(:direction, direction)},
          name: {:via, Registry, {Registry.RPCStreams, process_name}}
        )

      {0, pid} ->
        debug_log(state, ["→", process_name, body])
        Process.send(pid, {:incmessage, body}, [])
    end
  end

  # Non-streaming requests
  def request(%{flags: %{stream: 0, reqno: reqno}, request: body}, state, change) do
    debug_log(state, ["request", reqno, "sync:", body])
    change
  end

  defp debug_log(state, bits),
    do: Logger.debug(Enum.join([Util.visual_ident(state.their_pk) | bits], " "))

  defp pid_string(pid), do: pid |> :erlang.pid_to_list() |> to_string
end
