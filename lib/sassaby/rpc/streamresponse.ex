defmodule Sassaby.RPC.StreamResponse do
  use GenServer
  require Logger

  @moduledoc """
  Local network pub announcements
  """

  def start_link(opts, keywords) do
    GenServer.start_link(__MODULE__, opts, keywords)
  end

  @impl true
  def init({%Sassaby.RPC.Flags{:type => 2} = flags, body, parent_state} = opts) do
    start_stream({flags, Jason.decode!(body), parent_state})
    {:ok, opts}
  end

  # Don't try to stream anything if they are supposed to be the source and the request originated here
  defp start_stream({_flags, %{"type" => "source"}, %{:direction => {:us, :local}}}), do: :ok

  defp start_stream(
         {_flags, %{"name" => ["gossip", "ping"], "args" => [%{"timeout" => to}]}, _ps}
       ) do
    ping_em(:os.system_time(:millisecond) + to)
  end

  defp start_stream({_flags, %{"name" => ["blobs", "createWants"]}, _ps}) do
    Process.send(self(), {:outmessage, %{}}, [])
  end

  defp start_stream(_) do
    Process.send(
      self(),
      {:stopmessage, %{"message" => "Unimplemented procedure called", "name" => "Error"}},
      []
    )
  end

  @impl true
  def handle_info({:continue, func}, state) do
    func.()
    {:noreply, state}
  end

  def handle_info(:shutdown, state) do
    {:stop, :normal, state}
  end

  def handle_info({:incmessage, _message}, state) do
    {:noreply, state}
  end

  def handle_info({:outmessage, message}, {flags, _body, parent_state} = state) do
    Process.send(
      parent_state.connection_pid,
      {:streamresp, flags |> Map.replace(:reqno, parent_state.thisreq), message},
      []
    )

    {:noreply, state}
  end

  def handle_info({:stopmessage, message}, {flags, _body, parent_state} = state) do
    Process.send(
      self(),
      {:streamresp, flags |> Map.replace(:stop, 1) |> Map.replace(:reqno, parent_state.thisreq),
       message},
      []
    )

    {:stop, :normal, state}
  end

  # Streamer stuff
  defp ping_em(until) do
    now = :os.system_time(:millisecond)

    cond do
      now < until ->
        Process.send(self(), {:outmessage, now}, [])
        Process.send_after(self(), {:continue, fn -> ping_em(until) end}, 31121)

      true ->
        Process.send(self(), {:stopmessage, now}, [])
    end
  end
end
