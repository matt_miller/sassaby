defmodule Sassaby.Util do
  @moduledoc """
  Utility functions
  """

  @doc """
  Create a binary of `n` `O` bytes.
  """
  def zeroes(n), do: <<0::unsigned-size(n * 8)>>

  @doc """
  Gets the next nonce
  """
  def get_nonce(state, which) do
    Map.get_and_update!(state, which, fn n -> {n, up_one(n)} end)
  end

  defp up_one(n) do
    n
    |> :binary.decode_unsigned()
    |> then(fn n -> n + 1 end)
    |> then(fn n -> <<n::big-unsigned-192>> end)
  end

  @doc """
  Create a nice visual representation of an identity binary
  """
  def visual_ident(key) do
    <<abbrev::binary-size(7), _::binary>> = Base.encode64(key)
    "~" <> abbrev
  end

  @doc """
  Wrap a request into a sendable packet based on the flags and state.
  """
  def wrap_rpc(
        body,
        state,
        {:response, %Sassaby.RPC.Flags{type: type} = theirflags}
      ) do
    obody = if type == 2, do: Jason.encode!(body), else: body

    outflags = theirflags |> Map.replace(:len, byte_size(obody))

    rpcbody = Sassaby.RPC.Flags.to_header_bytes(outflags) <> obody

    {hnonce, up_state} = get_nonce(state, :send_nonce)
    {bnonce, final_state} = get_nonce(up_state, :send_nonce)
    msgbox = Kcl.secretbox(rpcbody, bnonce, state.send_key)
    <<bat::binary-size(16), justbody::binary>> = msgbox

    {Kcl.secretbox(<<byte_size(justbody)::unsigned-16>> <> bat, hnonce, state.send_key) <>
       justbody, final_state, outflags}
  end

  def wrap_rpc(body, state, {:request, ourflags}) do
    {reqno, req_state} = Map.get_and_update!(state, :our_request, fn n -> {n, n + 1} end)
    wrap_rpc(body, req_state, {:response, ourflags |> Map.replace(:reqno, reqno)})
  end

  def box_goodbye(state) do
    {hnonce, up_state} = get_nonce(state, :send_nonce)
    {Kcl.secretbox(zeroes(18), hnonce, state.send_key), up_state}
  end
end
