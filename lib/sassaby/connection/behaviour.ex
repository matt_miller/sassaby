defmodule Sassaby.Connection do
  @callback process_data(binary, tuple) :: tuple
  require Logger
  alias Sassaby.RPC.Dispatch
  alias Sassaby.Util

  @req_ms 503
  @stream_req_flags %Sassaby.RPC.Flags{type: 2, stream: 1}

  @doc false
  def ssb_hash(val), do: :crypto.hash(:sha256, val)
  defp ssb_hash(val1, val2), do: ssb_hash(val1 <> val2)

  @doc false
  def wrap_secret(secret, key), do: secret |> ssb_hash |> ssb_hash |> ssb_hash(key)

  def handle_info({:streamresp, flags, msg}, state) do
    {packet, new_state, _} = msg |> Util.wrap_rpc(state, {:response, flags})
    Logger.debug(["→ ", Util.visual_ident(state.their_pk), " res: ", Jason.encode!(msg)])
    send_packet(packet, new_state)
    {:noreply, new_state}
  end

  def handle_info({:streaminit, flags, msg}, %{:stream => :box} = state) do
    {packet, new_state, new_flags} = msg |> Util.wrap_rpc(state, {:request, flags})

    Dispatch.request(
      %{flags: new_flags, request: Jason.encode!(msg)},
      new_state,
      {:us, :local}
    )

    Logger.debug(["→ ", Util.visual_ident(state.their_pk), " req: ", Jason.encode!(msg)])
    send_packet(packet, new_state)
    {:noreply, new_state}
  end

  def handle_info({:streaminit, _, _} = msg, state) do
    Process.send_after(self(), msg, @req_ms)
    {:noreply, state}
  end

  def handle_info({:tcp_closed, _socket}, state) do
    Logger.info([Util.visual_ident(state.their_pk), " closed socket"])
    {:stop, :normal, state}
  end

  def handle_info({:initrequests, []}, state), do: {:noreply, state}

  def handle_info({:initrequests, [r | rest]}, state) do
    Process.send(self(), {:streaminit, @stream_req_flags, r}, [])
    Process.send_after(self(), {:initrequests, rest}, @req_ms, [])
    {:noreply, state}
  end

  def handle_info(:close, :ok) do
    {:stop, :shutdown, :ok}
  end

  def handle_info(:close, state) do
    Logger.info(["→ ", Util.visual_ident(state.their_pk), " goodbye"])
    {packet, new_state} = Util.box_goodbye(state)
    send_packet(packet, new_state)
    close_socket(state)
    {:stop, :shutdown, new_state}
  end

  defp send_packet(packet, %{:transport => nil, :socket => sock}), do: :gen_tcp.send(sock, packet)
  defp send_packet(packet, %{:transport => trans, :socket => sock}), do: trans.send(sock, packet)

  defp close_socket(%{:transport => nil, :socket => sock}), do: :gen_tcp.close(sock)
  defp close_socket(%{:transport => trans, :socket => sock}), do: trans.close(sock)

  def handshake_to_boxstream(state, box_data, module) do
    Logger.info([Util.visual_ident(state.their_pk), " handshake complete: ", to_string(module)])

    ss =
      state.network_id <>
        state.secret_ab <> state.secret_aB <> state.secret_Ab

    state
    |> Map.drop([
      :secret_ab,
      :secret_aB,
      :secret_Ab,
      :our_epk,
      :our_esk,
      :their_epk,
      :network_id,
      :our_pk,
      :our_sk
    ])
    |> Map.merge(%{
      :stream => :box,
      :recv_key => wrap_secret(ss, state.our_pk),
      :send_key => wrap_secret(ss, state.their_pk),
      :box_data => box_data
    })
  end
end
