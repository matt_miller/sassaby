defmodule Sassaby.Connection.Incoming do
  use GenServer, restart: :transient
  require Logger
  @behaviour Sassaby.Connection
  @behaviour :ranch_protocol

  @req_ms 503
  @init_requests [
    %{"name" => ["blobs", "createWants"], "args" => [], "type" => "source"},
    %{"name" => ["gossip", "ping"], "args" => [%{"timeout" => 300_000}], "type" => "duplex"}
  ]

  alias Sassaby.{Connection, Util}

  @impl true
  def start_link(ref, transport, opts) do
    {:ok, :proc_lib.spawn_link(__MODULE__, :init, [{ref, transport, opts}])}
  end

  @impl true
  def init({ref, transport, [hs_map]}) do
    {:ok, socket} = :ranch.handshake(ref)
    :ok = transport.setopts(socket, active: :once)

    state =
      Map.merge(hs_map, %{
        :connection_pid => self(),
        :our_request => 1,
        :socket => socket,
        :transport => transport,
        :network_id => Sassaby.network_id(),
        :our_pk => Sassaby.public_identity(:raw),
        :our_sk => Sassaby.secret_identity(:raw),
        :boxstream => :pack,
        :box_data => <<>>,
        :rpc_data => <<>>,
        :rpc_flags => <<>>
      })

    Process.send_after(self(), {:initrequests, @init_requests}, @req_ms, [])

    :gen_server.enter_loop(__MODULE__, [], state)
  end

  @impl true
  def handle_info({:tcp, socket, data}, state) do
    state.transport.setopts(socket, active: :once)
    {:noreply, process_data(data, state)}
  end

  # Psuedo-trait generic from the behaviour
  def handle_info(msg, state), do: Connection.handle_info(msg, state)

  @impl true
  def process_data(data, %{:socket => socket, :transport => transport} = state) do
    case Map.get(state, :stream) do
      :recv_hello ->
        <<chmac::binary-size(32), cepk::binary-size(32), rest::binary>> = state.box_data <> data

        case Kcl.valid_auth?(chmac, cepk, state.network_id) do
          true ->
            {esk, epk} = Kcl.generate_key_pair(:encrypt)
            shmac = Kcl.auth(epk, state.network_id)
            <<send_nonce::binary-size(24), _::binary>> = chmac
            <<recv_nonce::binary-size(24), _::binary>> = shmac
            transport.send(socket, shmac <> epk)

            Map.merge(state, %{
              :recv_nonce => recv_nonce,
              :send_nonce => send_nonce,
              :their_epk => cepk,
              :our_epk => epk,
              :our_esk => esk,
              :secret_ab => Curve25519.derive_shared_secret(esk, cepk),
              :secret_aB =>
                Curve25519.derive_shared_secret(Kcl.sign_to_encrypt(state.our_sk, :secret), cepk),
              :stream => :recv_auth,
              :box_data => rest
            })

          false ->
            Map.merge(state, %{:boxstream => :goodbye})
        end

      :recv_auth ->
        try do
          <<box::binary-size(112), rest::binary>> = state.box_data <> data

          <<sig::binary-size(64), cpk::binary-size(32)>> =
            Kcl.secretunbox(
              box,
              Sassaby.Util.zeroes(24),
              Connection.ssb_hash(state.network_id <> state.secret_ab <> state.secret_aB)
            )

          true =
            Kcl.valid_signature?(
              sig,
              state.network_id <> state.our_pk <> Connection.ssb_hash(state.secret_ab),
              cpk
            )

          ident = Base.encode64(cpk)

          case Sassaby.peer_status(ident) do
            :ignore ->
              Logger.info([Util.visual_ident(cpk), " connecting too frequently"])
              Process.send(self(), :close, [])

            :ok ->
              :ets.insert(:peers, {ident, Time.utc_now()})
          end

          secret_Ab =
            Curve25519.derive_shared_secret(
              state.our_esk,
              Kcl.sign_to_encrypt(cpk, :public)
            )

          state = Map.merge(state, %{:their_pk => cpk, :secret_Ab => secret_Ab})
          msg = state.network_id <> sig <> cpk <> Connection.ssb_hash(state.secret_ab)
          detached_signature_B = Kcl.sign(msg, state.our_sk)

          newbox =
            Kcl.secretbox(
              detached_signature_B,
              Sassaby.Util.zeroes(24),
              Connection.ssb_hash(
                state.network_id <>
                  state.secret_ab <> state.secret_aB <> state.secret_Ab
              )
            )

          transport.send(socket, newbox)

          Connection.handshake_to_boxstream(state, rest, __MODULE__)
        rescue
          e ->
            Logger.warn(Exception.format(:error, e, __STACKTRACE__))
            Process.send(self(), :close, [])
        end

      :box ->
        Sassaby.Boxstream.process(%{state | box_data: state.box_data <> data})
    end
  end
end
