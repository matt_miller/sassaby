defmodule Sassaby.Connection.Outgoing do
  use GenServer, restart: :transient
  require Logger
  @behaviour Sassaby.Connection

  alias Sassaby.Connection

  @req_ms 503
  @init_requests [
    %{"name" => ["blobs", "createWants"], "args" => [], "type" => "source"},
    %{"name" => ["gossip", "ping"], "args" => [%{"timeout" => 300_000}], "type" => "duplex"}
  ]

  def start_link(%{"id" => id} = opt_map) do
    GenServer.start_link(__MODULE__, opt_map, name: {:global, id})
  end

  @impl true
  def init(%{"id" => id, "port" => port, "ip" => ip}) do
    their_pk = Base.decode64!(id)
    {:ok, their_ip} = ip |> :binary.bin_to_list() |> :inet.parse_address()

    {:ok, socket} = :gen_tcp.connect(their_ip, String.to_integer(port), [:binary, active: :once])
    nid = Sassaby.network_id()
    {esk, epk} = Kcl.generate_key_pair()
    hmac = Kcl.auth(epk, nid)
    <<recv_nonce::binary-size(24), _::binary>> = hmac
    :gen_tcp.send(socket, hmac <> epk)

    state = %{
      :connection_pid => self(),
      :stream => :send_hello,
      :recv_nonce => recv_nonce,
      :our_request => 1,
      :socket => socket,
      :transport => nil,
      :network_id => Sassaby.network_id(),
      :their_pk => their_pk,
      :our_pk => Sassaby.public_identity(:raw),
      :our_epk => epk,
      :our_esk => esk,
      :our_sk => Sassaby.secret_identity(:raw),
      :boxstream => :pack,
      :box_data => <<>>,
      :rpc_data => <<>>,
      :rpc_flags => <<>>
    }

    Process.send_after(self(), {:initrequests, @init_requests}, @req_ms, [])

    {:ok, state}
  end

  @impl true
  def handle_info({:tcp, socket, data}, state) do
    :inet.setopts(socket, active: :once)
    {:noreply, process_data(data, state)}
  end

  # Psuedo-trait generic from the behaviour
  def handle_info(msg, state), do: Connection.handle_info(msg, state)

  @impl true
  def process_data(data, %{:socket => socket} = state) do
    case Map.get(state, :stream) do
      :send_hello ->
        <<chmac::binary-size(32), cepk::binary-size(32), rest::binary>> = state.box_data <> data

        case Kcl.valid_auth?(chmac, cepk, state.network_id) do
          true ->
            <<send_nonce::binary-size(24), _::binary>> = chmac
            secret_ab = Curve25519.derive_shared_secret(state.our_esk, cepk)

            secret_aB =
              Curve25519.derive_shared_secret(
                state.our_esk,
                Kcl.sign_to_encrypt(state.their_pk, :public)
              )

            secret_Ab =
              Curve25519.derive_shared_secret(
                Kcl.sign_to_encrypt(state.our_sk, :secret),
                cepk
              )

            msg = state.network_id <> state.their_pk <> Connection.ssb_hash(secret_ab)
            detached_signature_A = Kcl.sign(msg, state.our_sk)

            box =
              Kcl.secretbox(
                detached_signature_A <> state.our_pk,
                Sassaby.Util.zeroes(24),
                Connection.ssb_hash(state.network_id <> secret_ab <> secret_aB)
              )

            :gen_tcp.send(socket, box)

            Map.merge(state, %{
              :send_nonce => send_nonce,
              :their_epk => cepk,
              :detached_signature_A => detached_signature_A,
              :secret_ab => secret_ab,
              :secret_aB => secret_aB,
              :secret_Ab => secret_Ab,
              :stream => :send_auth,
              :box_data => rest
            })

          false ->
            Map.merge(state, %{:boxstream => :goodbye})
        end

      :send_auth ->
        try do
          <<box::binary-size(80), rest::binary>> = state.box_data <> data

          <<detached_signature_B::binary-size(64)>> =
            Kcl.secretunbox(
              box,
              Sassaby.Util.zeroes(24),
              Connection.ssb_hash(
                state.network_id <> state.secret_ab <> state.secret_aB <> state.secret_Ab
              )
            )

          true =
            Kcl.valid_signature?(
              detached_signature_B,
              state.network_id <>
                state.detached_signature_A <> state.our_pk <> Connection.ssb_hash(state.secret_ab),
              state.their_pk
            )

          Connection.handshake_to_boxstream(state, rest, __MODULE__)
        rescue
          e ->
            Logger.warn(Exception.format(:error, e, __STACKTRACE__))
            Process.send(self(), :close, [])
        end

      :box ->
        Sassaby.Boxstream.process(%{state | box_data: state.box_data <> data})
    end
  end
end
