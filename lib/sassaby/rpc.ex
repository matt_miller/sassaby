defmodule Sassaby.RPC do
  require Logger
  alias Sassaby.Util
  alias Sassaby.RPC.{Dispatch, Flags}

  def process(%{:rpc_flags => %Flags{} = flags, :rpc_data => msg} = state) do
    state_change =
      case flags do
        %Sassaby.RPC.Flags{reqno: 0, len: 0} ->
          Logger.debug([Util.visual_ident(state.their_pk), " embedded goodbye header"])
          %{:boxstream => :goodbye}

        %Sassaby.RPC.Flags{len: blen, reqno: reqno} = flags when byte_size(msg) >= blen ->
          <<rpc_bod::binary-size(blen), xs::binary>> = msg

          which_side = if reqno < 0, do: :us, else: :them
          Dispatch.request(%{flags: flags, request: rpc_bod}, state, {which_side, :network})

          %{:rpc_data => xs, :rpc_flags => <<>>, :boxstream => :pack}

        _ ->
          %{:rpc_data => msg, :boxstream => :pack}
      end

    newstate = state |> Map.merge(state_change)

    {:ok, newstate}
  end

  def process(%{:rpc_data => msg} = state) when byte_size(msg) >= 9 do
    case Flags.from_header_bytes(msg) do
      {%Sassaby.RPC.Flags{} = flags, more} ->
        process(%{state | :rpc_flags => flags, :rpc_data => more})

      _ ->
        %{:rpc_data => msg, :boxstream => :rpc}
    end
  end

  def process(state), do: {:ok, state}
end
