import Config

config :sassaby,
  settings_dir: "~/.sassaby",
  port: 8888,
  identity: "default",
  network: "main"
